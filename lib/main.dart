import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());
var points =0;
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
//      theme: ThemeData(
//        primarySwatch: Colors.blue,
//      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>  with SingleTickerProviderStateMixin{
  bool firstbox = false;
   bool two = false;
    bool three= false;
    bool four = false;
    bool  five= false;
    bool six = false ;
    bool seven = false;
  var temp = 0;
  TabController _tabController ;

  String get data => null;

  //Adding Points



  void add(var val){
    temp = temp + val;
  }

  // Sub Points

  void sub (var val){
    temp = temp -val;
    if(temp<0){
      temp =0;
    }

  }


  // Widget for CheckBox


 Widget CheckBoxTiles( String Title, bool Value){

    print(Value);
   return   CheckboxListTile(value:Value , onChanged: (newvlue){
     setState(() {
       switch(Title){

         case "Avoid processed food for a day":
                firstbox = newvlue;
                print(Value);
                if(newvlue) {
                add(1);
                }
                else{
            sub(1);
                }

           break;

          case "food for a day":
               two = newvlue;
               print(Value);
               if(newvlue) {
                add(1);
               }
               else{
                sub(1);
               }
           break;

         case "Do a 10-min workout":
           three = newvlue;
           print(Value);
           if(newvlue) {
             add(1);
           }
           else{
             sub(1);
           }
           break;

         case "Give up sugar for a day":
           four = newvlue;
           print(Value);
           if(newvlue) {
             add(1);
           }
           else{
             sub(1);
           }
           break;

         case "CheckBox5":
           five = newvlue;
           print(Value);
           if(newvlue) {
             add(2);
           }
           else{
             sub(2);
           }
           break;

         case "CheckBox6":
           six = newvlue;
           print(Value);
           if(newvlue) {
             add(2);
           }
           else{
             sub(2);
           }
           break;

         case "CheckBox7":
           seven = newvlue;
           print(Value);
           if(newvlue) {
             add(2);
           }
           else{
             sub(2);
           }
           break;


       }


        });

   },title: Text(Title),activeColor: Colors.blue,checkColor: Colors.white,controlAffinity: ListTileControlAffinity.trailing,);}


  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 3, vsync: this);

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
          titleSpacing: 25,
            title:Text('Points',style: TextStyle(color: Colors.blue),),
        actions: <Widget>[
          Center(child: InkWell(onTap: (){
            setState(() {

             if(temp <0)
              temp=0;
             else
               points = temp;
            });
          },child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.check), onPressed: null),
                Text("Submit", style: TextStyle(fontSize: 24,color: Colors.blue),),
              ],
            ),
          )),

          ) ],
      ),
      body:SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: new  Container(
          height: double.maxFinite,
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 9,
                ),
                Text("Your Monthly Points",style: TextStyle(fontSize: 25),),
                SizedBox(
                  height: 35,
                ),
                InkWell(
//                onTap: () => setState((){}),
                  /*child: Container(child: Center( child: Text(points.toString(),style: TextStyle(fontSize: 25,color: Colors.blue),), ),*/

                  child: Container(child: Column(children: <Widget>[ SizedBox(height: 15,),Center( child: Text(points.toString(),style: TextStyle(fontSize: 35,color: Colors.blue))),
                    Center( child: Text('Points',style: TextStyle(fontSize: 25,color: Colors.blue))),              ],),
                    height: 150,
                    width: 150,
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(1.0, 1.5),
                          blurRadius: 1.5,
                        )
                      ]
                    ),
                  ),
                ),
                SizedBox(height: 35,),
                Text(" Hai This Is A Sample Of Content", style: TextStyle(fontSize: 20),),
                SizedBox(
                  height: 9,
                ),

          DefaultTabController( initialIndex: 0,length: 2, child: Column(children: [
            Container(

              constraints: BoxConstraints.expand(height: 50),
              child: TabBar(tabs: [
                Tab(child: Column(children: <Widget>[
                  Text('Silver',style: TextStyle(color: Colors.blue)),
                  Text('(1 Points)',style: TextStyle(color: Colors.blue)),
                ],)),
                Tab(child: Column(children: <Widget>[
                  Text('Gold',style: TextStyle(color: Colors.blue)),
                  Text('(2 Points)',style: TextStyle(color: Colors.blue)),
                ],))
              ],),


            ),
           Container(
             height: 350,
             child: TabBarView(children: <Widget>[
               Container(child: Column(
                 children: <Widget>[


                   CheckBoxTiles('Avoid processed food for a day', firstbox),
                   CheckBoxTiles('food for a day', two),
                   CheckBoxTiles('Do a 10-min workout', three),
                   CheckBoxTiles('Give up sugar for a day', four),

  /*               CheckboxListTile(value: firstbox, onChanged: (newvlue){
                     setState(() {
                       firstbox = newvlue;
                       if(firstbox){
                         temp = temp +5;
                       }
                       else{
                        temp = temp-5;
                       }
                     });

                   },title: Text('Avoid processed food for a day'),activeColor: Colors.blue,checkColor: Colors.blueGrey,controlAffinity: ListTileControlAffinity.trailing,),

                   CheckboxListTile(value: three, onChanged: (newvlue){
                     setState(() {
                       three = newvlue;
                       if(three){
                         temp = temp +5;
                       }
                       else{
                        temp= temp-5;
                       }
                     });

                   },title: Text(' food for a day'),activeColor: Colors.blue,checkColor: Colors.blueGrey,controlAffinity: ListTileControlAffinity.trailing,),
                   CheckboxListTile(value: two, onChanged: (newvlue){
                     setState(() {
                       two = newvlue;
                       if(two){
                         temp = temp +5;
                       }
                       else{
                       temp =   temp-5;
                       }
                     });

                   },title: Text('Avoid  a day'),activeColor: Colors.blue,checkColor: Colors.blueGrey,controlAffinity: ListTileControlAffinity.trailing,),


*/


                 ],
               )),
               Container(
                   height: 250,
                   child: Column(
                 children: <Widget>[

                   CheckBoxTiles('CheckBox5', five),
                   CheckBoxTiles('CheckBox6', six),
                   CheckBoxTiles('CheckBox7', seven),


                   /*             CheckboxListTile(value: four, onChanged: (newvlue){
                     setState(() {
                       four = newvlue;
                       if(four){
                         temp = temp +10;
                       }
                       else{
                         temp =   temp-10;
                       }
                     });

                   },title: Text('Avoid  a day'),activeColor: Colors.blue,checkColor: Colors.amberAccent,controlAffinity: ListTileControlAffinity.trailing,),
                   CheckboxListTile(value: five, onChanged: (newvlue){
                     setState(() {
                       five = newvlue;
                       if(five){
                         temp = temp +10;
                       }
                       else{
                         temp =   temp-10;
                       }
                     });

                   },title: Text('Avoid  a day'),activeColor: Colors.blue,checkColor: Colors.white,controlAffinity: ListTileControlAffinity.trailing,),

                   CheckboxListTile(value: six, onChanged: (newvlue){
                     setState(() {
                       six = newvlue;
                       if(six){
                         temp = temp +10;
                       }
                       else{
                         temp =   temp-10;
                       }
                     });

                   },title: Text('Avoid  a day'),activeColor: Colors.blue,checkColor: Colors.white,controlAffinity: ListTileControlAffinity.trailing,),
*/




                 ],
               )),
             ],),
           )


          ],))















              ],
            ),
          ),
        ),
      ),
    );
  }
}
